package mail

// the mail package handles sending emails. The code in this package should be
// restricted to that which generates and sends the email based on provided
// information. Any code that gathers the data or information for the email
// should be managed in the package where the data and/or information is coming
// from.
//
// NOTE: It is set up to send emails via gmail and the email addresses are
// currently hardcoded to be from and to mjsevey@gmail.com

import (
	"crypto/tls"
	"fmt"
	"net/smtp"
	"os"
	"strings"

	"github.com/MSevey/traderbot/metrics"
)

var (
	gmailPassword = os.Getenv("gmailPassword")

	// SenderEmail is the Hard Coded sender Email
	SenderEmail = "mjsevey@gmail.com"

	// ToEmail is the Hard Coded To email address
	ToEmail = "mjsevey@gmail.com"
)

// Mail contains the information about the email being sent
type Mail struct {
	SenderID string
	ToIds    []string
	Subject  string
	Body     string
}

// SMTPServer is the server settings for emailing
type SMTPServer struct {
	host string
	port string
}

// performanceToBody creates a body for an email from the portfolio performance
// struct
func performanceToBody(performance metrics.PortfolioPerformance) string {
	var body string
	for _, a := range performance.Assets {
		s := fmt.Sprintf(`
		Asset: %v
		Absolute Qty Increase: %v
		Percent Qty Increase: %v
		Absolute Value Increase: %v
		Percent Value Increase: %v
		
		`, a.Symbol, a.QtyIncreaseAbs, a.QtyIncreasePercent, a.ValueIncreaseAbs, a.ValueIncreasePercent)
		body += s
	}
	return body
}

// EmailLifeTimePerformance sends an email with the lifetime performance of the
// portfolio
func EmailLifeTimePerformance() error {
	// Get performance information
	performance, err := metrics.LifeTimePortfolioPerformance()
	if err != nil {
		return err
	}

	// Build Mail
	mail := Mail{
		SenderID: SenderEmail,
		ToIds:    []string{ToEmail},
		Subject:  "Lifetime Performance",
		Body:     performanceToBody(performance),
	}

	// Send mail
	return SendEmail(mail)
}

// SendEmail sends an email, based on the provided Mail parameters
//
// NOTE: currently hardcoded for Gmail
func SendEmail(mail Mail) error {
	// Build message
	messageBody := mail.BuildMessage()

	// Set mail server parameters
	smtpServer := SMTPServer{host: "smtp.gmail.com", port: "465"}

	// build authorization
	auth := smtp.PlainAuth("", mail.SenderID, gmailPassword, smtpServer.host)

	// Gmail will reject connection if it's not secure
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.host,
	}
	conn, err := tls.Dial("tcp", smtpServer.ServerName(), tlsconfig)
	if err != nil {
		return err
	}

	// Build client
	client, err := smtp.NewClient(conn, smtpServer.host)
	if err != nil {
		return err
	}

	// Use Authorization
	if err = client.Auth(auth); err != nil {
		return err
	}

	// add all from and to addresses and initiate mail
	if err = client.Mail(mail.SenderID); err != nil {
		return err
	}
	for _, k := range mail.ToIds {
		if err = client.Rcpt(k); err != nil {
			return err
		}
	}

	// Write Data to body
	w, err := client.Data()
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(messageBody))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}

	// Close the Client
	err = client.Quit()
	if err != nil {
		return err
	}

	return nil
}

// ServerName returns the constructed name of the server
func (s *SMTPServer) ServerName() string {
	return s.host + ":" + s.port
}

// BuildMessage builds the email message
func (m *Mail) BuildMessage() string {
	var message string
	message += fmt.Sprintf("From: %s\r\n", m.SenderID)
	if len(m.ToIds) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(m.ToIds, ";"))
	}

	message += fmt.Sprintf("Subject: %s\r\n", m.Subject)
	message += "\r\n" + m.Body

	return message
}
